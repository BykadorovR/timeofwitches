//
// Created by Roman on 02.04.2016.
//

#ifndef TIMEOFWITCHES_SPRITE_H
#define TIMEOFWITCHES_SPRITE_H


class Sprite {
    int x;
    int y;
    int width;
    int height;
public:
    Sprite(int _x, int _y, int _width, int _height);
};


#endif //TIMEOFWITCHES_SPRITE_H
