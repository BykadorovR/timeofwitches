#include "platform_asset_utils.h"
#include "macros.h"
#include "platform_log.h"
#include <android/asset_manager_jni.h>
#include <assert.h>
#include <android/log.h>
#include <string>
#include <algorithm>

static AAssetManager* asset_manager;
using namespace std;

const string TAG = "asset";

extern "C" {
    void Java_com_jiffy_timeofwitches_PlatformFileUtils_init_1asset_1manager(JNIEnv *env, jclass jclazz,
                                                                         jobject java_asset_manager) {
        UNUSED(jclazz);
        asset_manager = AAssetManager_fromJava(env, java_asset_manager);
        assert(asset_manager != NULL);
    }
}
FileData get_asset_data(const char* relative_path) {
	assert(relative_path != NULL);
    __android_log_print(ANDROID_LOG_VERBOSE, TAG.c_str(), "Input file: %s\n", (relative_path));
	AAsset* asset = AAssetManager_open(asset_manager, relative_path, AASSET_MODE_STREAMING);
	assert(asset != NULL);
    int length = AAsset_getLength(asset);
    __android_log_print(ANDROID_LOG_VERBOSE, TAG.c_str(), "Shader source size: %d\n", length);
    //own realization of logging with exluding \r symbols
    log_text_file(TAG, (char*)AAsset_getBuffer(asset), length);
    return (FileData) { AAsset_getLength(asset),(const char*)AAsset_getBuffer(asset), (const char*)asset };
}

void release_asset_data(const FileData* file_data) {
	assert(file_data != NULL);
	assert(file_data->file_handle != NULL);
	AAsset_close((AAsset*)file_data->file_handle);
}
