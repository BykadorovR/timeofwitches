#include "platform_log.h"
#include <stdio.h>
#include <stdlib.h>

const int max_output_size = 1000;

void log_text_file(std::string tag, const char* text, const int length) {
    if (length < max_output_size) {
        std::string result_buffer;
        for (int i = 0; i < length; i++) {
            if (text[i] != '\r')
                result_buffer.push_back(text[i]);
        }
        __android_log_print(ANDROID_LOG_VERBOSE, tag.c_str(), "From text file:\n%s\n",
                result_buffer.c_str());
    } else {
        __android_log_print(ANDROID_LOG_VERBOSE, tag.c_str(),
                "From text file:\nThe max size of text file for display is: %d\nBut you have: %d\n",
                           max_output_size, length);
    }
}

void crash() {
    __builtin_trap();

}

void log_v(std::string tag, std::string text) {
    if (LOGGING_ON) {
        __android_log_print(ANDROID_LOG_VERBOSE, tag.c_str(), " \n%s\n",
                            text.c_str());
    }
}

void log_d(std::string tag, std::string text) {
    if (LOGGING_ON) {
        __android_log_print(ANDROID_LOG_DEBUG, tag.c_str(), " \n%s\n",
                            text.c_str());
    }
}