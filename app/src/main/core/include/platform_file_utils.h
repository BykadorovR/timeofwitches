#pragma once
typedef struct {
	const long data_length;
	const char* data;
	const char* file_handle;
} FileData;
