#include "config.h"
#include <string>
#include <android/log.h>

void log_text_file(std::string tag, const char* text, const int length);
void crash();
//Here need to add templates for %d,%s,%c ..
void log_v(std::string tag, std::string text);
void log_d(std::string tag, std::string text);