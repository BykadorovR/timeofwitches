LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := timeofwitches
LOCAL_LDLIBS := -lGLESv2 -llog -landroid
LOCAL_CPPFLAGS    := -Wall -Wextra
# To build the whole .so
FILE_LIST := $(wildcard $(LOCAL_PATH)/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../core/src/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../objects/src/*.cpp)
LOCAL_SRC_FILES += $(FILE_LIST:$(LOCAL_PATH)/%=%)
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../core/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../objects/include
LOCAL_STATIC_LIBRARIES := libpng
include $(BUILD_SHARED_LIBRARY)
$(call import-add-path,$(LOCAL_PATH)/../3rdparty)
$(call import-module,libpng)