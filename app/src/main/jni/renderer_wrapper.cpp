#include "game.h"
#include "macros.h"
#include <jni.h>

/* These functions are called from Java. */
using namespace std;
extern "C" {
    void Java_com_jiffy_timeofwitches_RendererWrapper_on_1surface_1created(JNIEnv *env, jclass cls) {
        UNUSED(env);
        UNUSED(cls);
        on_surface_created();
    }

    void Java_com_jiffy_timeofwitches_RendererWrapper_on_1surface_1changed(JNIEnv *env, jclass cls,
                                                                           jint width, jint height) {
        UNUSED(env);
        UNUSED(cls);
        UNUSED(width);
        UNUSED(height);
        on_surface_changed();
    }

    void Java_com_jiffy_timeofwitches_RendererWrapper_on_1draw_1frame(JNIEnv *env, jclass cls) {
        UNUSED(env);
        UNUSED(cls);
        on_draw_frame();
    }
}